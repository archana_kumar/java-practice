package com.bitbucket.javapractice.calculators;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.assertj.core.api.Assertions.*;

/**
 * Created by archana on 7/21/16.
 */

@RunWith(DataProviderRunner.class)
public class MiscCalculatorsTest {
    // @formatter:off
    @Test
    @DataProvider(value = {
            "0              |  0",
            "fizz           |  3",
            "buzz           |  5",
            "fizzbuzz       |  15",
    }, splitBy = "\\|", trimValues = true)
    // @formatter:on
    public void testStringLength2(String str, int testNumber) {
        assertThat(MiscCalculators.fizzBuzz(testNumber)).isEqualTo(str);

    }

    @Test
    public void joinIntegers() throws Exception {

    }

    @Test
    public void rootMeanSquare() throws Exception {

    }

}