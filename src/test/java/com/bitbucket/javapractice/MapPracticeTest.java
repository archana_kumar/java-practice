package com.bitbucket.javapractice;


import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class MapPracticeTest {

    @Test
    public void basicMap() {

        Map<String, Integer> firstMap = new HashMap<>();
        firstMap.put("a", 1);
        firstMap.put("b", 2);
        firstMap.put("c", 3);
        assertThat("Error", firstMap.containsKey("b"), is(true));
        assertThat("Error", firstMap.containsValue(1), is(true));
        assertThat("Not a Map", firstMap.size(), is(3));
        assertThat("Not a Map", firstMap.containsKey("a"), is(true));
        assertThat("Not a Map", firstMap.isEmpty(), is(false));


    }

    @Test
    public void mapCollectExample() {

        Map<String, Integer> firstMap = new HashMap<>();
        Map<Integer, String> secondMap = new HashMap<>();
        firstMap.put("a", 1);
        firstMap.put("b", 2);
        firstMap.put("c", 3);

        firstMap.forEach((k, v) -> secondMap.put(v, k));
        assertThat("Error", secondMap.containsKey(1), is(true));
        assertThat("Error", secondMap.containsValue("a"), is(true));
        assertThat("Not a Map", secondMap.size(), is(3));
        assertThat("Not a Map", secondMap.isEmpty(), is(false));


    }


    @Test
    public void mapTransformExample() {


        Map<Integer, List<String>> secondMap = new HashMap<>();
        secondMap.put(1, Arrays.asList("A", "B"));
        secondMap.put(2, Arrays.asList("C", "D"));

        Map<String, Integer> thirdMap = new HashMap<>();
        secondMap.forEach((k, v) -> v.forEach(y -> thirdMap.put(y, k)));
        assertThat("Error", thirdMap.size(), is(4));
        assertThat("Error", thirdMap.get("A"), is(1));
        secondMap.forEach((k, v) ->
                v.forEach(y -> assertThat(thirdMap.get(y), is(k))));


    }
}
