package com.bitbucket.javapractice;


import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;
import java.util.stream.IntStream;

import static org.hamcrest.core.Is.*;
import static org.junit.Assert.*;

public class ListPracticeTest {
    @Test
    public void basicList() {
        List<Integer> lst = new ArrayList <> ();
        IntStream.range(0, 20).forEach(x -> lst.add (x));
        assertThat("List is not the right size", lst.size(), is(20));
        assertThat("List is not the right size", lst.get(0), is(0));
        assertThat("List is not the right size", lst.get(lst.size()-1), is(19));
        assertThat("List is not the right size", lst.indexOf(5), is(5));
        assertThat("List is not the right size", lst.indexOf(50), is(-1));
        assertThat("List is not the right size", lst.contains(7), is(true));

    }

    @Test
    public void loopList() {
        List<Integer> lst = new ArrayList<>();
        IntStream.range(0, 20).forEach(x -> lst.add(x));
        int count = 0;
        for (Integer item : lst) {
            count++;
        }
        assertThat("List is not the right size", count, is(20));
        assertThat("List is not the right size", lst.stream().count(), is(20L));
        assertThat("List is not the right size", lst.stream().filter(x -> x % 10 == 0).count(), is(2L));

    }

    @Test
    public void basicSet() {

        Set<Integer> firstSet = new HashSet<> ();
        firstSet.add(1);
        firstSet.add(1);
        firstSet.add(1);
        firstSet.add(1);
        firstSet.add(2);
        firstSet.add(2);
        firstSet.add(3);
        firstSet.add(3);
        firstSet.add(1);
        assertThat("Error", firstSet.contains(2), is(true));
        assertThat("Error", firstSet.size(), is(3));
        assertThat("Error", firstSet.contains(0), is(false));


    }


}
