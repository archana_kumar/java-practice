package com.bitbucket.javapractice;

import com.bitbucket.javapractice.calculators.InterestCalculator;
import org.assertj.core.data.Offset;
import org.junit.Test;
import static org.assertj.core.api.Assertions.*;

/**
 * Created by archana on 7/21/16.
 */
public class InterestCalculatorTest {
    @Test
    public void calculateSimpleInterest(){
        Double interest = InterestCalculator.simpleInterest(100.0, 10.0, 5.0 );
        assertThat(interest).as("interest failed").isCloseTo(50.0, Offset.offset(0.01));
    }



}
