Create a new class WorkoutPlanner.
It has a method : getTodaysWorkout.
Input : Get's today's date from System date.
Output : enum : Workout

enum Workout {
    RUNNING, WALKING, REST, CYCLING, WEIGHT_TRAINING;
}

monday , wednesday : RUNNING
tuesday, thu : WEIGHT_TRAINING
friday : CYCLING
saturday : WALKING
sunday : REST

Use Data Provider for all days : http://testng.org/doc/documentation-main.html#parameters-dataproviders

Time Api to get today's day : https://docs.oracle.com/javase/8/docs/api/java/time/LocalDate.html

To get current LocalDate : LocalDate.now(); ( Static method in API ).
Then find the right method to use to get day of week