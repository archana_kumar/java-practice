package com.bitbucket.javapractice.caloriecounter;

import java.util.Collections;
import java.util.List;

/**
 * Calculate total calories for a member of the program.
 * Total calories = total of food items - total of activities.
 * If total >= calorie target, then member has met the target.
 * Null values are not allowed for foodItems or Activities, but they can be empty
 *
 */
public class Member {

    private final int calorieTarget;
    private final List<Activity> activities;
    private final List<FoodItem> foodItems;

    public int getCalorieTarget() {
        return calorieTarget;
    }

    public List<FoodItem> getFoodItems() {
        return Collections.unmodifiableList(foodItems);
    }

    public List<Activity> getActivities() {
        return Collections.unmodifiableList(activities);
    }


    public Member(int calorieTarget, List<Activity> activities, List<FoodItem> foodItems) {
        if( activities == null || foodItems.isEmpty()) {
            throw new IllegalArgumentException("activities cannot be Null");
        }

        if( foodItems.isEmpty()) {
            throw new IllegalArgumentException("foodItems cannot be Null");
        }

        this.calorieTarget = calorieTarget;
        this.activities = activities;
        this.foodItems = foodItems;
    }

    public int totalCalories () {
        return foodItems.stream().mapToInt( x -> x.calories).sum()
                + activities.stream().mapToInt( x -> x.calories).sum();
    }

    public boolean isTargetAchieved() {
        return totalCalories() >= calorieTarget;
    }

}
