package com.bitbucket.javapractice.caloriecounter;

/**
 * Created by amit on 7/14/16.
 */
public abstract class CalorieSource {
    protected final String description;
    protected final int calories;

    public CalorieSource(String description, int calories) {
        this.description = description;
        this.calories = calories;
    }

}
