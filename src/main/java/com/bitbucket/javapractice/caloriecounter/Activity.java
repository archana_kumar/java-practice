package com.bitbucket.javapractice.caloriecounter;

/**
 * Created by amit on 7/14/16.
 */
public  class Activity extends CalorieSource {

    public Activity(String description, int calories) {
        super(description, calories);
    }
}
