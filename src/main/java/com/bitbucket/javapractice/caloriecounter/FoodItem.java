package com.bitbucket.javapractice.caloriecounter;

/**
 * Created by amit on 7/14/16.
 */
public class FoodItem extends CalorieSource {

    public FoodItem(String description, int calories) {
        super(description, calories);
    }
}
