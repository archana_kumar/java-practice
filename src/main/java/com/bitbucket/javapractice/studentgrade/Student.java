package com.bitbucket.javapractice.studentgrade;

/**
 */
public class Student {
    private final String id;
    private final Double gpa;
    private final int numberOfExtraCredits;

    public Student(String id, Double gpa, int numberOfExtraCredits) {
        this.id = id;
        this.gpa = gpa;
        this.numberOfExtraCredits = numberOfExtraCredits;
    }

    public int getNumberOfExtraCredits() {
        return numberOfExtraCredits;
    }

    public Double getGpa() {
        return gpa;
    }

    public String getId() {
        return id;
    }

}
