package com.bitbucket.javapractice.studentgrade;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.lang.Math.max;

/**
 * Grade : >90.0 = A, >80+ -> 90 = B, 70+ -> 80 = C, 50 to 70 = D ,< 50 F
 * extra credit = 5% upto max of 100.0
 */

public class GradeCalculator {

    public enum Grade {A, B, C, D, F}

    ;

    public Map<Student, Grade> calculateGrades(List<Student> studentList) {
        return studentList.stream().collect(Collectors.toMap(Function.identity(), this::calculateGrade));
    }

    public Double adjustedGpa(Student student) {

        return max((student.getGpa() + student.getNumberOfExtraCredits() * 1.05), 100.0);

    }

    public Grade calculateGrade(Student student) {
        Grade grade = Grade.F;
        Double adjustedGpa = adjustedGpa(student);
        if (adjustedGpa > 90.0) {
            grade = Grade.A;
        } else if (adjustedGpa <= 90.0 && adjustedGpa > 80.0) {
            grade = Grade.B;
        } else if (adjustedGpa <= 80.0 && adjustedGpa > 70.0) {
            grade = Grade.C;
        } else if (adjustedGpa <= 700.0 && adjustedGpa >= 50.0) {
            grade = Grade.D;
        }
        return grade;
    }

}
