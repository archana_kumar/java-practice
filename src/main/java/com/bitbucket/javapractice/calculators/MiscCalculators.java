package com.bitbucket.javapractice.calculators;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.lang.Math.sqrt;
import static java.util.stream.Collectors.joining;

/**
 * Created by amit on 7/15/16.
 */
public class MiscCalculators {

    private MiscCalculators() {}

    public static final String FIZZ = "fizz";
    public static final String BUZZ = "buzz";

    public static String fizzBuzz (int numToTest) {
        boolean divBy3 = numToTest %3 == 0;
        boolean divBy5 = numToTest %5 == 0;
        if ( divBy3 && divBy5 ) {
            return FIZZ + BUZZ;
        } else if( divBy3) {
            return FIZZ;
        } else if ( divBy5) {
            return FIZZ;
        } else {
            return String.valueOf( numToTest);
        }

    }

    public  static String joinIntegers(List<Integer> listOfInts) {
        return  listOfInts.stream()
                .map(Object::toString)
                .collect(joining(", "));
    }


    public  static Double rootMeanSquare(List<Double> listOfInts) {
        return sqrt(listOfInts.stream()
                .mapToDouble(x -> x * x)
                .sum());
    }

}
