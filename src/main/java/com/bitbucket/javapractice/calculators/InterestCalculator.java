package com.bitbucket.javapractice.calculators;

/**
 */
public class InterestCalculator {

    private InterestCalculator() {}
    public static Double simpleInterest(Double principal, Double percentRatePerYear, Double numYears ) {
        return principal * numYears * ( percentRatePerYear/100.0);

    }

}
